window.onload = () => {
    document.getElementById("load").addEventListener("click", async () => {
        const response = await fetch("./data/example.json");
        teachers = await response.json();
        console.log(teachers);
        let tableBody = document.querySelector("table>tbody");
        tableBody.innerHTML = "";
        for (let i = 0; i < teachers.length; i++) {
            let row = tableBody.insertRow(i);
            var idCell = row.insertCell(0);
            var emailCell = row.insertCell(1);
            var nameCell = row.insertCell(2);
            idCell.innerHTML = teachers[i]["id"];
            emailCell.innerHTML = teachers[i]["email"];
            nameCell.innerHTML = teachers[i]["name"];
        }
    });
}