window.onload = function() {
    console.log("Window loaded");
    let myButton = document.getElementById("my-button");
    myButton.addEventListener("click", change_paragraph);
}

function change_paragraph() {
    console.log("Loading paragraph");
    var text = document.getElementById('my-input-text');
    var paragraph = document.getElementById('change-me');
    console.log(text.value);
    paragraph.innerHTML = text.value;
}

