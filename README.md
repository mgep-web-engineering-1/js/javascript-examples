# JavaScript Examples

## 1 - Output Data

We are going to see 3 options to output data using JavaScript:

### 1.1 - Alerts

```javascript
alert("This is an alert");
```

This will open a windows with a message.

**Not very recommended as they are intrusive and can be blocked.**

### 1.2 - Debug console log

```javascript
console.log("This is a console log message.");
```

This will write a message in a console.

Regular users won't see it, but any advanced user could see it. **Do not write any sensitive information here!**

### 1.3 - Write in the DOM

```javascript
var paragraph = document.getElementById("change-me");
paragraph.innerHTML = "New text";
```

More complex output.

Non intrusive, the user can see the output.

You can change many things, even add new HTML elements.

```window.onload``` is needed, otherwise JS is loaded before HTML and "change-me" paragraph is not found.

## 2 - Input data

### 2.1 - Prompts

They work similar to alerts, but they have an input text.

```javascript
let text = prompt("Write your name");
```

### 2.2 - Buttons

You can get users' events, for example, you can do something when the user clicks a button:

```html
<button onclick="change_paragraph()">Change paragraph</button>
<p id="change-me">Default value</p>
```

```javascript
function change_paragraph(){
  document.getElementById("change-me").innerHTML = "Changed using JS";
}
```

### 2.3 - Input text

You can interact with the user getting text from inputs:

```javascript
var inputText = document.getElementById('my-input-text');
paragraph.innerHTML = inputText.value;
```

In the first line we get the input object, and in the second line we extract it's content and add it to the paragraph. With inputs we have to use ```.value``` instead of ```innerHTML```.

## 3 - Separating JavaScript from HTML

We should separate the JavaScript code from the HTML code, so is easier to maintain, increase reusability and so we have the logic and the structure separated.

### 3.1 - External JS file

Instead of using the ```<script>``` tag with the entire JS code, we can add it to link to an external JS file.

```html
<script src="js/myexternalscript.js"></script>
```

Even if it feels like that, script tags are **not** self closing tags, so we cannot do things like ~~```<script src="js/myexternalscript.js"/>```~~.

### 3.2 - Add events in the JS file

If we want to completely remove JS code from HTML, we also have to remove event attributes.

```html
<button ... onload="change_paragraph()">...</button>
```

In order to do that, we can use the ```addEventListener``` function.

```javascript
window.onload = function() {
    console.log("Window loaded");
    let myButton = document.getElementById("my-button");
    myButton.addEventListener("click", change_paragraph);
}
```

Again, we are using ```ẁindow.onload``` so the button is loaded before the ```addEventListener``` function is executed, otherwise we will have an error message telling us that myButton is **null**.

### 4.1 -  Fetch API

Beste leku batean dauden datuak jaso nahi baditugu (REST api bat, JSON fitxategi bat...) Fetch APIa erabili dezakegu.

Async/await erabiltzen du, beraz JavaScript haria ez da itxaroten gelditzen erantzuna jaso arte, beste ebentu batzuei erantzuten joan daiteke:

```javascript
const response = await fetch("./data/example.json");
teachers = await response.json();
```

Funtzioaren definizioa ere aldatzen da, eta ```async``` eduki beharko du funtzio asinkronoa dela definitzeko, bestela ezin izango du ```await``` egin.

```javascript
document.getElementById("load").addEventListener("click", async () => {
    ...
}
```
